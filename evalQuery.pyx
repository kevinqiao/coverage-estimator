from BitVector import BitVector
import lark

#returns index of sastifing constraint
def evalQuery(tree, env, sharedTreeCache, sharedTreeEnv):

    #checks if result has already be computed
    if type(tree) == str:
        if tree in sharedTreeCache:
            return sharedTreeCache[tree]
        toRet = evalQuery(sharedTreeEnv[tree], env, sharedTreeCache, sharedTreeEnv)
        sharedTreeCache[tree] = toRet
        return toRet
    data = tree.data
    cdef int val
    cdef int s
    cdef int index
    cdef int offset
    cdef int length, i

    if data == 'comp':
        com = tree.children[0]
        op = {True:BitVector(intVal=1, size=1), False:BitVector(intVal=0, size=1)}
        l = evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv)
        r = evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv)

        if com == 'Eq':
            return op[l==r]
        if com == 'Slt':
            return op[l<r]
        elif com == 'Sgt':
            return op[l>r]
        elif com == 'Sle':
            return op[l>=r]
        elif com == 'Sge':
            return op[l>=r]
        elif com == 'Ult':
            return op[l<r]
        elif com == 'Ugt':
            return op[l>r]
        elif com == 'Ule':
            return op[l<=r]
        elif com == 'Uge':
            return op[l>=r]
        raise Exception('Missed all comp')

    elif data == 'arith':
        com = tree.children[0]
        l = evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv)
        r = evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv)
        val = 0

        if com == 'Add':
            val = l.intValue() + r.intValue()
        elif com == 'Sub':
            val = l.intValue() - r.intValue()
        elif com == 'Mul':
            val = l.intValue() * r.intValue()
        elif com == 'Udiv':
            val = l.intValue() / r.intValue()
        return BitVector(intVal = val, size=int(tree.children[1][1:]))

    elif data == 'const':
        t = tree.children[0].children[0].type
        val = 0
        if t == 'DEC':
            val = int(tree.children[0].children[0])
        elif t == "HEX":
            val = int(tree.children[0].children[0], 16)
        elif t == "OCT":
            val = int(tree.children[0].children[0], 8);
        elif t == "BIN":
            val = int(tree.children[0].children[0], 2)
        return BitVector(intVal=val, size=32)

    elif data == 'number':
        return evalQuery(tree.children[0], env, sharedTreeCache, sharedTreeEnv)

    elif data == 'typenumber':
        val = evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv).intValue()
        s = int(tree.children[0].value[1:])
        return BitVector(intVal = val, size = s)

    elif data == 'bitwise':
        com = tree.children[0]
        if com == 'Or':
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv) | evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv)
        elif com == 'And':
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv) & evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv)
        elif com == 'Xor':
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv) ^ evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv)
        elif com == 'Shl':
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv) << evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv).intValue()
        elif com == 'LShr':
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv).shift_right(evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv).intValue())
        elif com == 'AShr':
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv) >> evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv).intValue()

    elif data == 'assign':
        env[tree.children[0]] = evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv)
        return env[tree.children[0]]

    elif data == 'identifier':
        return env[tree.children[0].value]

    elif data == 'concat':
        left = evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv)
        right = evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv)

        return left + right

    elif data == 'extract':
        s = int(tree.children[0][1:])
        offset = int(tree.children[1].children[0].children[0].value)

        bv = evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv)
        return bv[bv.size-offset-s:bv.size-offset]

    elif data == 'zext':
        length = int(tree.children[0][1:])
        res = evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv)
        if res.size < length:
            return BitVector(intVal = res.intValue(), size = length)
        return res[-length:]

    elif data == 'read':
        s = int(tree.children[0][1:])
        index = evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv).intValue()
        version = tree.children[2].children[0]
        if index == 0:
            return env[version[:17]][-8:]
        else:
            return env[version[:17]][-8*(index+1):-8*index]

    elif data == 'select':

        if evalQuery(tree.children[1], env, sharedTreeCache, sharedTreeEnv).intValue() == 1:
            return evalQuery(tree.children[2], env, sharedTreeCache, sharedTreeEnv)
        else:
            return evalQuery(tree.children[3], env, sharedTreeCache, sharedTreeEnv)
    elif data == 'readlsb':
        s = int(tree.children[0][1:])
        index = evalQuery(tree.children[1].children[0], env, sharedTreeCache, sharedTreeEnv).intValue()
        version = tree.children[2].children[0]
        val = 0
        if index == 0:
            val = env[version[:17]][-s:].intValue()
        else:
            val = env[version[:17]][:32].intValue()
        return BitVector(size=s, intVal = val)

    #query commands
    elif data == 'constraint_list':
        if len(tree.children) == 0:
            return False
        for c in tree.children:
            if evalQuery(c, env, sharedTreeCache, sharedTreeEnv).intValue() == 0:
                return False
        return True

    elif data == 'array_declaration':
        if len(tree.children) == 4:
            return
        if tree.children[0].value in env:
            return
        hexS = tree.children[4].children[0].value.replace('0x','').replace(' ','')
        env[tree.children[0].value] = BitVector(hexstring = hexS)
        return env[tree.children[0].value]

    elif data == 'query':
        return evalQuery(tree.children[len(tree.children)-1], env, sharedTreeCache, sharedTreeEnv)

    elif data == 'query_command':
        return evalQuery(tree.children[0], env, sharedTreeCache, sharedTreeEnv)

    elif data == 'query_expression':
        raise Exception('Entered query expression')
    raise Exception('Bad parse')
