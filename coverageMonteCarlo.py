from BitVector import BitVector
from multiprocessing import Process, Array

import multiprocessing
import random
import time
import larkParser
import preprocess
import evalQuery
import sys

#runs monte carlo n times
def monteCarlo(n, totalCount):
    env = {}
    env['true'] = BitVector(intVal=1, size=1)
    env['false'] = BitVector(intVal=0, size=1)
    evalQuery.evalQuery(tree.children[0].children[0], env, None, sharedTreeEnv)    #stores constant array value

    for i in range(n):
        sharedTreeCache = {}
        env['v0_Symbolic_Delay'] = BitVector(intVal=random.randint(0, 1024), size=64)
        env['v1_Symbolic_Delay'] = BitVector(intVal=random.randint(0, 1024), size=64)
        env['v2_Symbolic_Delay'] = BitVector(intVal=random.randint(0, 1024), size=64)
        env['v3_Symbolic_Delay'] = BitVector(intVal=random.randint(0, 1024), size=64)
        '''
        env['v4_Symbolic_Delay'] = BitVector(intVal=random.randint(0, 1024), size=64)
        env['v5_Symbolic_Delay'] = BitVector(intVal=random.randint(0, 1024), size=64)
        '''
        for j in range(len(tree.children)):
            if evalQuery.evalQuery(tree.children[j], env, sharedTreeCache, sharedTreeEnv):
                totalCount[j] += 1
                break

tree = larkParser.larkParser(sys.argv[1])
sharedTreeEnv = preprocess.preprocess(tree)

if __name__ == '__main__':

    ti = time.time()
    totalCount = Array('i', [0]*(len(tree.children)+1))
    numThreads = 8
    nPerThread = 100
    nSamples = 1000
    ctr = 0
    for j in range(nSamples):

        rti = time.time()
        threads = []
        for i in range(numThreads):
            t = Process(target=monteCarlo, args=(nPerThread, totalCount))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()

        ct = time.time()
        print(totalCount[:])
        print((j+1)*numThreads*nPerThread)
        print(ct - ti)
        print(ct - rti)
