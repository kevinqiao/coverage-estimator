import re
import sys
text = open(sys.argv[1], 'r').read().split('\n')
newText = []
query = []
discard = False

#finds const_arr1
for i in range(len(text)):
    t = text[i]
    pattern = re.compile(r'^array const_arr1.+')
    matches = pattern.findall(t)
    if len(matches) == 1:
        newText.append(t)
        break

#removes out of range queries
for i in range(len(text)):
    t = text[i]
    pattern = re.compile(r'^[ ]+message: \"End of Simulation\"')
    matches = pattern.findall(t)
    if len(matches) == 1:
        query = []
        discard = False
        query.append(t)
    else:

        query.append(t)
        pattern = re.compile(r'^[ ]+message: \"Out of Range\"')
        matches = pattern.findall(t)
        if len(matches) == 1:
            discard = True
        else:

            pattern = re.compile(r'^\d+ \[State \d+\] State was .+')
            matches = pattern.findall(t)
            if len(matches) == 1 or i == len(text)-1:
                if not(discard):
                    for s in query:
                        pattern = re.compile(r'^array.+')
                        matches = pattern.findall(s)
                        if len(matches) == 0:
                            newText.append(s)

#removes irrelevant info
output = open(sys.argv[2], 'w')
for t in newText:
    pattern = re.compile(r'\d+ \[State \d+\] [\d\w\(\)\[\]!=\' ]+')
    matches = pattern.findall(t)
    if len(matches) == 0:
        pattern = re.compile(r'^(TestCaseGenerator|Constraints|State ): [\w\d\t:,\"\'\->=\[\] ]+$')
        matches = pattern.findall(t)
        if len(matches) == 0:
            pattern = re.compile(r'^[ ]+(message|status): [\w\d\t:,\"\' ]+$')
            matches = pattern.findall(t)
            if len(matches) == 0:
                pattern = re.compile(r'^[ ]*v\d_Symbolic_Delay_\d+: [\d\w]+ [\d\w]+ .+')
                matches = pattern.findall(t)
                if len(matches) == 0:
                    pattern = re.compile(r'^Creating plugin \w+')
                    matches = pattern.findall(t)
                    if len(matches) == 0:
                        pattern = re.compile(r'^Initing initial device state\.')
                        matches = pattern.findall(t)
                        if len(matches) == 0:
                            pattern = re.compile(r'^SymbExpression.+')
                            matches = pattern.findall(t)
                            if len(matches) == 0:
                                pattern = re.compile(r'^[ ]+\(ReadLSB w32 0x0 v\d_Symbolic_Delay_\d+\)\)$')
                                matches = pattern.findall(t)
                                if len(matches) == 0:
                                    pattern = re.compile(r'^WARNING.+')
                                    matches = pattern.findall(t)
                                    if len(matches) == 0:
                                        output.write(t)
                                        output.write('\n')
