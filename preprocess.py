import lark
counter = 0

#returns string representation of tree
def dfs(tree):
    if type(tree) == lark.lexer.Token:
        return str(tree.value)[:17]
    toReturn = str(tree.data)
    for i in range(0, len(tree.children)):
        toReturn = toReturn + ' ' + dfs(tree.children[i])
    return toReturn

#reshapes tree to remove duplicate part
def reshape(tree, treeEnv, sharedTreeEnv):
    if type(tree) == lark.lexer.Token or tree.data == 'version' or tree.data == 'identifer' or tree.data == 'index_expression' or tree.data == 'const':
        return tree
    s = dfs(tree)
    if s in treeEnv:
        return treeEnv[s]
    global counter
    treeEnv[s] = 'Tree' + str(counter)
    sharedTreeEnv[treeEnv[s]] = tree
    counter+=1
    for i in range(0, len(tree.children)):
        tree.children[i] = reshape(tree.children[i], treeEnv, sharedTreeEnv)
    return tree

#reshapes all queries in tree
def preprocess(tree):

    treeEnv = {}
    sharedTreeEnv = {}
    for i in range(0, len(tree.children)):
        tree.children[i].children[-1].children[0] = reshape(tree.children[i].children[-1].children[0], treeEnv, sharedTreeEnv)
    return sharedTreeEnv
