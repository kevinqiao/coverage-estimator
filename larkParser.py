from lark import Lark

#Lark grammer
np = Lark('''
    querylist: query+
    query: array_declaration? query_command
    array_declaration: "array" IDENTIFIER "[" number "]" ":" TYPE "->" TYPE "=" "[" number_list "]"
        | "array" IDENTIFIER "[" number "]" ":" TYPE "->" TYPE "=" "symbolic"
    number_list: /[\w\d ]+/
    query_command: "(" "query" constraint_list query_expression eval_expr_list eval_array_list ")"
        | "(" "query" constraint_list query_expression ")"
    constraint_list: "[" expression* "]"
    eval_expr_list: "[" expression* "]"
    eval_array_list: "[" IDENTIFIER* "]"
    query_expression: expression
    ?expression: "(" COMP TYPE* expression expression ")" -> comp
        | "(" ARITH TYPE expression expression ")" -> arith
        | "(" BITWISE TYPE expression expression ")" -> bitwise
        | "(" "ReadLSB" TYPE index_expression version ")" -> readlsb
        | "(" "Concat" TYPE expression expression ")" -> concat
        | "(" "Extract" TYPE number expression ")" -> extract
        | "(" "ZExt" TYPE expression ")" -> zext
        | "(" "SExt" TYPE expression ")" -> sext
        | "(" "Read" TYPE expression version ")" -> read
        | "(" "Select" TYPE expression expression expression ")" -> select
        | number -> number
        | "(" TYPE number ")" -> typenumber
        | IDENTIFIER ":" expression -> assign
        | IDENTIFIER -> identifier
    index_expression: number
    BITWISE: "And" | "Or" | "Xor" | "Shl" | "LShr" | "AShr"
    ARITH: "Add"| "Sub"| "Mul"| "UDiv"| "URem"| "SDiv"| "SRem"
    number: TRUEFALSE -> truefalse | signed_constant -> const
    TRUEFALSE: "true" | "false"
    signed_constant: DEC | BIN | OCT | HEX
    DEC: /[0-9_]+/
    BIN: /0b[01_]+/
    OCT: /0o[0-7_]+/
    HEX: /0x[0-9a-fA-F_]+/
    TYPE: /w[0-9]+/
    IDENTIFIER: /[a-zA-Z_][a-zA-Z0-9._]*/
    version: IDENTIFIER
    COMP: "Eq"
        | "Ne"
        | "Ult"
        | "Ule"
        | "Ugt"
        | "Uge"
        | "Slt"
        | "Sle"
        | "Sgt"
        | "Sge"
    %import common.WS
    %ignore WS
''', start='querylist', parser='lalr', lexer='contextual')

#parses kquery log file and returns tree
def larkParser(fname):
    queryText = open(fname).read()
    return np.parse(queryText)
