from BitVector import BitVector
from multiprocessing import Process, Array, Value

import multiprocessing
import time
import larkParser
import preprocess
import evalQuery
import sys

#1 dim brute force
def bruteForce(totalCount, v1=0, v2=0, v3=0, v4=0, v5=0):
    env = {}
    env['true'] = BitVector(intVal=1, size=1)
    env['false'] = BitVector(intVal=0, size=1)
    evalQuery.evalQuery(tree.children[0].children[0], env, None, sharedTreeEnv)    #stores constant array value

    for i in range(1025):
        sharedTreeCache = {}
        env['v0_Symbolic_Delay'] = BitVector(intVal=i, size=64)
        env['v1_Symbolic_Delay'] = BitVector(intVal=v1, size=64)
        '''
        env['v2_Symbolic_Delay'] = BitVector(intVal=v2, size=64)
        env['v3_Symbolic_Delay'] = BitVector(intVal=v3, size=64)
        env['v4_Symbolic_Delay'] = BitVector(intVal=v4, size=64)
        env['v5_Symbolic_Delay'] = BitVector(intVal=v5, size=64)
        '''
        for j in range(len(tree.children)):
            if evalQuery.evalQuery(tree.children[j], env, sharedTreeCache, sharedTreeEnv):
                totalCount[j] += 1
                break

tree = larkParser.larkParser(sys.argv[1])
sharedTreeEnv = preprocess.preprocess(tree)

if __name__ == '__main__':
    
    ti = time.time()
    totalCount = Array('i', [0]*(len(tree.children)+1))
    numThreads = 1
    for v1 in range(0, 128):

        rti = time.time()
        threads = []
        for i in range(numThreads):
            t = Process(target=bruteForce, args=(totalCount, 8*v1+i))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()
        ct = time.time()
        print(totalCount[:])
        print(1025*numThreads*(v1+1))
        ct = time.time()
        print(ct - ti)
        print(ct - rti)
    t = Process(target=bruteForce, args=(totalCount, 1024))
    t.start()
    t.join()
    ct = time.time()
    print(totalCount[:])
    print(ct - ti)
